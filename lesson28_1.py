## Substitute strings


import re
nameRegex = re.compile(r'Agent \w+')
nameRegex.findall('Agent Alice gave the secret docuement to Agent Bob.')

nameRegex.sub('Redacted', 'Agent Alice gave the secret docuement to Agent Bob.')


## Slash number syntax to convert numbers
namesRegex = re.compile(r'Agent (\w)\w*')
namesRegex.findall('Agent Alice gave the secret docuement to Agent Bob.')
namesRegex.sub(r'Agent \1****', 'Agent Alice gave the secret docuement to Agent Bob.')

## VERBOSE
re.compile(r'''
(\d\d\d\)|  # area code (without parens, with dash)
(\(\d\d\d)) # -or- area code wiht parens and no dash    
-           # First Dash
\d\d\d      # first 3 digtis
-           # second dash
\d\d\d\d    # last 4 digits
\sx\d{2,4}  #extension, like x1234''', re.IGNORECASE | re.VERBOSE | re.DOTALL)