#! /usr/bin/env python3

def box_print(symbol, width, height):
    if len(symbol) !=1:
        raise Exception('"symbol" needs to be  a string of lenght 1.')
    if (width < 2) or (height < 2):
        raise Exception('"width" and "height" must be bigger or equal to two')
    
    print(symbol * width)
    
    for i in range(height - 2):
        print(symbol + (' ' * (width - 2)) + symbol)

    print(symbol * width)


box_print('*', 15, 5)
box_print('**', 15, 5)


import traceback

try:
    raise Exception('teste de erros!')
except:
    error_file = open('error_log.txt', 'a')
    error_file.write(traceback.format_exc())
    error_file.close()
    print('The traceback was written to the log')

    
