import re
beginsWithHelloREgex = re.compile(r'^Hello')
beginsWithHelloREgex.search('Hello There')

beginsWithHelloREgex.search('He Said "Hello"')

endsWithHelloREgex = re.compile(r'world!$')
endsWithHelloREgex.search('hello world!')

# all didigts
allDigitsRegex = re.compile(r'^\d+$')

allDigitsRegex.search('1237621487368917263487623478623489761234987')
allDigitsRegex.search('12376214873689172634x87623478623489761234987') == None

atRegex = re.compile(r'.at')
atRegex.findall('The cat in the hat sat in the flat fat mat')

atRegex = re.compile(r'.{1,2}at')
atRegex.findall('The cat in the hat sat in the flat fat mat')

'First Name: Al Last Name: Sweigart'.find(':')+2



nameRegex = re.compile(r'First Name: (.*) Last Name: (.*)')
nameRegex.findall('First Name: Al Last Name: Sweigart')

.*?

serve = '<to serve humans> for dinner.>'

nongreedy = re.compile(r'<(.*?)>')
nongreedy.findall(serve)

greedy = re.compile(r'<(.*)>')
greedy.findall(serve)


## new line dotall

prime = 'Serve the public trust.\nProtect the inocent.\nUpload the law'
print(prime)


dotStar = re.compile(r'.*')
dotStar.search(prime)


dotStar = re.compile(r'.*', re.DOTALL)


vowellRegex = re.compile(r'[aeiou]')
vowellRegex.findall('Al why does your programming dmenu
book talk about RoboCop so much?')

vowellRegex = re.compile(r'[aeiou]', re.IGNORECASE)