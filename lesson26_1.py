import re

phoneRegex = re.compile(r'\d\d\d\-\d\d\d-\d\d\d\d')
phoneRegex


## returns a simple group
resume='''My phone number is 555-444-1231, but my home phone is 444-555-4124, what about you?'''
phoneRegex.findall(resume)

## returns tuples if enclosed in parens

phoneRegex = re.compile(r'(\d\d\d)-(\d\d\d-\d\d\d\d)')
phoneRegex.findall(resume)

phoneRegex = re.compile(r'((\d\d\d)-(\d\d\d-\d\d\d\d))')

## Character classes

digitRegex = re.compile(r'\d')

lyrics = '11 Pipers Piping, 10 Lords a Leaping, 9 Ladies Dancing, 8 Maids a Milking, 7 Swans a Swimming, 6 Geese a Laying, 5 Golden Rings, 4 Calling Birds, 3 French Hens, 2 Turtle Doves, and 1 Partridge in a Pear Tree'

xmasRegex = re.compile(r'\d+\s\w+')

xmasRegex.findall(lyrics)

## personal characther classes
vowelRegex = re.compile(r'[aeiouAEIOU]')
vowelRegex.findall('Robocop eats baby food! 345Chupaççççç')


vowelRegex = re.compile(r'[aeiouAEIOU]{2}')
vowelRegex.findall('Robocop eats baby food! 345Chupaççççç')


## negative character classes
vowelRegex = re.compile(r'[^aeiouAEIOU]{2}')
vowelRegex.findall('Robocop eats baby food! 345Chupaççççç')