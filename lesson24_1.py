#! /usr/bin/python3

import re

phoneNumRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
phoneNumRegex.search('My Number is 415-555-4242')

mo = phoneNumRegex.search('My Number is 415-555-4242')
mo.group()


## Groups
phoneNumRegex = re.compile(r'(\d\d\d)-(\d\d\d-\d\d\d\d)')
mo = phoneNumRegex.search('My Number is 415-555-4242')
mo.group()
mo.group(1)
mo.group(2)


## finding individual characters
frase = re.compile(r'\ç')
mo = frase.search('Gonçalo')
mo.group()


batRegex = re.compile(r'Bat(man|mobile|copter|bat)')
mo = batRegex.search('Batmobile lost a wheel')
mo.group()

## None value is return if the regex doesn't find

# group() returns the found expression, gropup(1) returns the found part of the group not the whole word
mo.group(1)