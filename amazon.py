#!/usr/bin/env python3


import bs4, requests


def get_amazon_price(product_url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',} 
    res = requests.get(product_url, headers=headers)
    res.raise_for_status()

    soup = bs4.BeautifulSoup(res.text, 'html.parser')
    elems = soup.select('#price_inside_buybox')
    return elems[0].text.strip()


price = get_amazon_price('https://www.amazon.de/tenoning-Jig-Gusseisen-f%C3%BCr-Tisch/dp/B07FKP87N4/')
print('The price is ' + price)
