## String Methods

### lower upper
spam = 'Hello World!'
spam.upper()
spam
spam.lower()


answer = input()
answer
if answer == 'yes':
    print('Playing again Sam!')


## islower isupper
spam = 'HEllo World!'
spam.islower()


'HEllO Motherfucker'.upper().isupper()

## Outros:  devovlem um resultado booleano
#  isalpha() - letras
#  isalnum() - letras e números apenas
#  isdecimal() - numeros apaneas
#  isspace() - whitespace
#  istitle() - titlecase only

spam.title()

'hello'.isalpha()
'123124'.isalpha()

## startswith endswith

'Hello world!'.startswith('Hello')
'Hello world!'.startswith('He'

'Hello world!'.endswith('world!')

## join()

','.join(['cats', 'rats','bats'])


## split()

'my name is simon'.split()
'my name is simon'.split('m')

# ljust rjust - padding strings to justify

'HEllo'.rjust(10)
'Hello motherfucker'.rjust(100,'*')

## center

'Hello'.center(20, '=')


## Strip, lstrip, rstrip
spam = '   HEllo  '.rjust(10)
spam.strip()
spam.lstrip()
spam.rstrip()

'spamspamspambaconSpermamamamps'.strip('amps')


spam = 'Hello there!'
spam.replace('e', 'XYZ')


## Pyperclip - copy and paste

import pyperclip

pyperclip.copy('hey motherfucker!!!')
pyperclip.paste()

