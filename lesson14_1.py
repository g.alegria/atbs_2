
## Ranges
""" range(5)

list(range(5))
list(range(0, 1000, 2 ))


spam = list(range(0, 10400, 27))
spam
 """


## Lists
supplies = ['pens', 'staplers', 'flame-throwers', 'binders']
for i in range(len(supplies)):
    print('Index ' + str(i) + ' in supplies is: '+ supplies[i])



## Multiple Assignement in lists
cat = ['fat', 'orange', 'lousd']
""" size = cat[0]
color = cat[1]
disposition = cat[2] """

size, color, disposition = cat
size


## Multiple variable to multiple values


size, color, disposition = 'skinny', 'black', 'quiet'
a = 'AAA'
b = 'BBB'

a, b = b, a

### Augmented assignement
spam = 4
spam += 1


### For Loops technically  iterate over the values in a list.

### The range() function returns a list-like valuer, wich can be passed to the list functions if you need an actual list value.

### VAriables can swap their values using multiple assignements.

### Augmented assignement operatros like += ares used as shortcuts.



