## list methods


spam = ['hello', 'hi', 'howdy', 'heyas']

## index method of the list type
spam.index('hello')

spam.index('heyas')

### throws a value error
spam.index('bajskdbakjdbskajsdb')

spam = ['Zophie', 'Pooka', 'Fat-tail', 'Pooka']
spam.index('Pooka')

## Append and Insert methods

spam = ['cat', 'dog', 'bat']
spam.append('moose')
spam


spam = ['cat', 'dog', 'bat']
spam.insert(2,'chicken')
spam

## remove method
spam = ['cat', 'bat', 'rat', 'elephant']
spam
spam.remove('bat')
spam

### del statement
del spam[0]
spam

### remove method only deletes the first occurence
spam = ['cat', 'bat', 'rat', 'elephant', 'cat', 'cat', 'cat', 'cat']
spam
spam.remove('cat')
spam

## sort method - ordena as listas, numéricas ou strings
spam = [2, 5, 3.14, 1, -7]
spam.sort()
spam

spam = ['ants', 'cats', 'dogs', 'elepnhats', 'aardvarks', 'aarpercolins']
spam.sort()
spam

### sort key argument: reverse
spam = ['ants', 'cats', 'dogs', 'elepnhats', 'aardvarks', 'aarpercolins']
spam.sort(reverse=True)
spam

### mixed sort doens't work
spam = [1, 2, 3, 'Alice', 'bob']
spam.sort()

### Sort works by ASCII values not alphabetical
spam = ['Ants', 'ants', 'cats', 'dogs', 'Dogs', 'elepnhats', 'aardvarks','Bums', 'aarpercolins']
spam.sort()
spam

### Sort needs a key argument in order to sort by true alphabetilcal order
spam = ['Ants', 'ants', 'cats', 'dogs', 'Dogs', 'elepnhats', 'aardvarks','Bums', 'aarpercolins']
spam.sort(key=str.lower)
spam