import os

for folder_name, sub_folders, files_names in os.walk('/home/gna/'):
    print('The folder is ' + folder_name)
    print('The subfolders in ' + folder_name + ' are: '+ str(sub_folders))
    print('The file names in ' + folder_name + ' are: '+ str(files_names))
    print()