## 19 String Operations

### Table 6-1: Escape Characters

## \' --- Single Quote
## \" --- Double Quote
## \t --- Tab
## \n --- Newline - Line break
## \\ --- Backslash


print('Hello there \n How are You?\n I\'m fine')

r'Hello'

r'Carol\'s cat'


print(r'Carol\'s cat')


## Multinline Strings

print("""Dear alice, eves cat has been arrrested fot catnnaping, cat burgulary
and extorition.
Sincerly,
Bob.''')


spam= """The Project Gutenberg EBook of Romeo and Juliet, by William Shakespeare





The Complete Works of William Shakespeare

The Tragedy of Romeo and Juliet

The Library of the Future Complete Works of William Shakespeare



1595

THE TRAGEDY OF ROMEO AND JULIET

by William Shakespeare



Dramatis Personae

  Chorus.


  Escalus, Prince of Verona.

  Paris, a young Count, kinsman to the Prince.

  Montague, heads of two houses at variance"""

print(len(spam))


'HEllo World!'
spam = 'Hello World'
spam[4]


