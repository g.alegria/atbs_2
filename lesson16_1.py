## Lists and strings: Similiarties


a = list('HEllo')
a
a[1:3]
a


## Strings are immutable

name = 'Zophie a cat'
newName = name[0:7] + 'the' + name[8:12]
newName

## list values are referenced lists

spam = [0, 1, 2, 3, 4, 5]
cheese = spam
cheese[1] = 'Hello!'
print(cheese[1])
spam
cheese

## function deepcopy from the copy module

import copy

spam = ['A', 'B', 'C', 'D']
cheese =  copy.deepcopy(spam)
cheese[1] = 42
cheese

## Line Continuation

spam = ['apples',
        'oranges',
        'bananas',
        'cats']
spam

print('Four score and seven' + \
 ' years ago')