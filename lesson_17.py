# DICTIONARIES

myCat = {'size': 'fat', 'color':'gray', 'disposition':'loud'}
myCat['size']

'My cat has ' + myCat['color'] + ' fur.'

## Dictionaries have no order
eggs = {'name': 'Zophie', 'species': 'cat', 'age': 8}
ham = {'name': 'Zophie', 'age': 8, 'species': 'cat'}
eggs == ham

>>> eggs == ham
True


##KEy Error
eggs['color']

'name' not in eggs

## keys(), values(), and items() Dcitionary methods

list(eggs.keys())
list(eggs.values())
list(eggs.items())

for k in eggs.keys():
    print(k)

for k, v in eggs.items():
    print(k, v)


## Get method
eggs
eggs.get('age', 0)
eggs.get('color', '')
picniicItems = {'apples': 5, 'cups':2}
print('I am bringing ' + str(picniicItems.get('napkins', 0)) + ' to the picnic')

## setdefault method creates a default for a key: value pair
eggs = {'name': 'Zophie', 'species': 'cat', 'age': 8}
eggs
if 'color' not in eggs:
    eggs['color'] = 'black'

eggs

eggs.setdefault('color', 'black')
eggs    
