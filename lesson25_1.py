import re

# ? matches the preceding group o or one times


batRegex = re.compile(r'Batman|Batwoman')
batRegex = re.compile(r'Bat(wo)?man(\!)?')
mo = batRegex.search('The Adventures of Batman, Batwoman and Robin, and also some participation by the lady woaman, Batwoman!')
mo




    phoneRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
    mo = phoneRegex.search('My phone is 415-555-1234. Call me Tomorrow')
    mo.group()



phoneRegex = re.compile(r'(\d\d\d-)?\d\d\d-\d\d\d\d')
mo = phoneRegex.search('My phone is 555-1234. Call me Tomorrow')
mo.group()


# * match 0 or more times

batRegex = re.compile(r'Bat(wo)*man')
batRegex.search('The Adventures of Batman, Batwoman and Robin, and also some participation by the lady woaman, Batwoman!')
batRegex.search('The Adventures of Batman and Robin, and also some participation by the lady woama!')


# + match one or more
batRegex = re.compile(r'Bat(wo)+man')


regex = re.compile(r'\+\*\?')
regex.search('I\'ve learned about +*? regex syntax')

regex = re.compile(r'(\+\*\?)+')
regex.search('I\'ve learned about +*?+*?+*?+*?+*?+*?+*?+*?+*?+*? regex +*?+*?+*?+*?+*?+*? syntax')



### Repetitions

haRegex = re.compile(r'(Ha){3}')
haRegex.search('He said HaHaHa')

phoneRegex = re.compile(r'((\d\d\d-)?\d\d\d-\d\d\d\d(,)?){3}')
phoneRegex.search('My numbers are 415-555-1234,555-4242,212-555-0000')
phoneRegex.search('My numbers are 415-555-1234,555-4242,212-555-000')
mo == None


import re
regex = re.compile(r'((\d\d\d-)?\d\d\d-\d\d\d\d(,)?){3}')
result = regex.search('My numbers are 415-555-1234,555-4242,212-555-0000')
print(result)