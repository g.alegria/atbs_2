import logging
logging.basicConfig(filename='log.txt', level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(message)s')
#logging.disable(logging.CRITICAL)


# five levels of warning. a
logging.debug('Start of program')


def factorial(n):
    logging.debug('Start of factorial(%s)' % (n))
    total = 1
    for i in range(1, n + 1):
        total *= i
        logging.info('i is %s, total is %s' % (i, total))
    logging.warning('Retrun value is %s' % (total))
    return total


print(factorial(5))
print(factorial(15))
logging.critical('End of Program')
